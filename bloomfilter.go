/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package leveldb

import (
	"crypto/sha256"
	"github.com/greatroar/blobloom"
	"hash/maphash"
)

type bloomFilter struct {
	f    *blobloom.SyncFilter
	seed maphash.Seed
}

func (b *bloomFilter) add(id []byte)      { b.f.Add(b.hash(id)) }
func (b *bloomFilter) has(id []byte) bool { return b.f.Has(b.hash(id)) }

// Hash function for the bloomfilter: maphash of the SHA-256.
//
// The randomization in maphash should ensure that we get different collisions
// across runs, so colliding keys are not kept indefinitely.
func (b *bloomFilter) hash(id []byte) uint64 {
	if len(id) != sha256.Size {
		panic("bug: bloomFilter.hash passed something not a SHA256 hash")
	}
	var h maphash.Hash
	h.SetSeed(b.seed)
	h.Write(id)
	return h.Sum64()
}
