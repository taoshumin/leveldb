/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package example

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/taoshumin/leveldb/backend"
	"testing"
)

func TestNamespace_Bool(t *testing.T) {
	db := backend.OpenMemory()
	defer db.Close()

	ns := NewNamespace(db, "foo")
	_ = ns.PutBool("hello1", true)
	_ = ns.PutString("hello2", "val1")

	v, _, _ := ns.String("hello1")
	t.Log("Str: ", v)

	// Write Transaction
	tr, err := ns.db.NewWriteTransaction()
	assert.NoError(t, err)
	defer tr.Release()

	it, err := tr.NewPrefixIterator([]byte(ns.prefix))
	if err != nil {
		return
	}

	for it.Next() {
		_ = tr.Delete(it.Key())
	}
	it.Release()
	_ = tr.Commit()
}
