/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package example

import (
	"encoding/binary"
	"gitlab.com/taoshumin/leveldb"
	"gitlab.com/taoshumin/leveldb/backend"
	"time"
)

type Namespace struct {
	db     backend.Backend
	prefix string
}

func NewNamespace(db backend.Backend, prefix string) *Namespace {
	return &Namespace{
		db:     db,
		prefix: prefix,
	}
}

// PutInt64 stores a new int64. Any existing value (even if of another type)
// is overwritten.
func (n *Namespace) PutInt64(key string, val int64) error {
	var valBs [8]byte
	binary.BigEndian.PutUint64(valBs[:], uint64(val))
	return n.db.Put(n.prefixedKey(key), valBs[:])
}

// Int64 returns the stored value interpreted as an int64 and a boolean that
// is false if no value was stored at the key.
func (n *Namespace) Int64(key string) (int64, bool, error) {
	valBs, err := n.db.Get(n.prefixedKey(key))
	if err != nil {
		return 0, false, filterNotFound(err)
	}
	val := binary.BigEndian.Uint64(valBs)
	return int64(val), true, nil
}

// PutTime stores a new time.Time. Any existing value (even if of another
// type) is overwritten.
func (n *Namespace) PutTime(key string, val time.Time) error {
	valBs, _ := val.MarshalBinary()
	return n.db.Put(n.prefixedKey(key), valBs)
}

// Time returns the stored value interpreted as a time.Time and a boolean
// that is false if no value was stored at the key.
func (n *Namespace) Time(key string) (time.Time, bool, error) {
	var t time.Time
	valBs, err := n.db.Get(n.prefixedKey(key))
	if err != nil {
		return t, false, filterNotFound(err)
	}
	err = t.UnmarshalBinary(valBs)
	return t, err == nil, err
}

// PutString stores a new string. Any existing value (even if of another type)
// is overwritten.
func (n *Namespace) PutString(key, val string) error {
	return n.db.Put(n.prefixedKey(key), []byte(val))
}

// String returns the stored value interpreted as a string and a boolean that
// is false if no value was stored at the key.
func (n *Namespace) String(key string) (string, bool, error) {
	valBs, err := n.db.Get(n.prefixedKey(key))
	if err != nil {
		return "", false, filterNotFound(err)
	}
	return string(valBs), true, nil
}

// PutBytes stores a new byte slice. Any existing value (even if of another type)
// is overwritten.
func (n *Namespace) PutBytes(key string, val []byte) error {
	return n.db.Put(n.prefixedKey(key), val)
}

// Bytes returns the stored value as a raw byte slice and a boolean that
// is false if no value was stored at the key.
func (n *Namespace) Bytes(key string) ([]byte, bool, error) {
	valBs, err := n.db.Get(n.prefixedKey(key))
	if err != nil {
		return nil, false, filterNotFound(err)
	}
	return valBs, true, nil
}

// PutBool stores a new boolean. Any existing value (even if of another type)
// is overwritten.
func (n *Namespace) PutBool(key string, val bool) error {
	if val {
		return n.db.Put(n.prefixedKey(key), []byte{0x0})
	}
	return n.db.Put(n.prefixedKey(key), []byte{0x1})
}

// Bool returns the stored value as a boolean and a boolean that
// is false if no value was stored at the key.
func (n Namespace) Bool(key string) (bool, bool, error) {
	valBs, err := n.db.Get(n.prefixedKey(key))
	if err != nil {
		return false, false, filterNotFound(err)
	}
	return valBs[0] == 0x0, true, nil
}

// Delete deletes the specified key. It is allowed to delete a nonexistent
// key.
func (n Namespace) Delete(key string) error {
	return n.db.Delete(n.prefixedKey(key))
}

func (n Namespace) prefixedKey(key string) []byte {
	return []byte(n.prefix + key)
}

func filterNotFound(err error) error {
	if backend.IsNotFound(err) {
		return nil
	}
	return err
}

// NewDeviceStatisticsNamespace return namespace for device statistics.
func NewDeviceStatisticsNamespace(db backend.Backend, device string) *Namespace {
	return NewNamespace(db, string(leveldb.KeyTypeDeviceStatistic)+device)
}

// NewFolderStatisticsNamespace return namespace for folder statistics.
func NewFolderStatisticsNamespace(db backend.Backend, folder string) *Namespace {
	return NewNamespace(db, string(leveldb.KeyTypeFolderStatistic)+folder)
}
