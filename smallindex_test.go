/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package leveldb

import (
	"encoding/binary"
	"github.com/stretchr/testify/assert"
	"gitlab.com/taoshumin/leveldb/backend"
	"testing"
)

func Test_newSmallIndex(t *testing.T) {
	db := backend.OpenLevelDBMemory()
	defer db.Close()

	idx := newSmallIndex(db, []byte{KeyTypeDeviceIdx})
	// 添加数据
	id, err := idx.ID([]byte("test1"))
	assert.NoError(t, err)
	t.Log("ID: ", id)

	// 根据id获取值
	v, ok := idx.Val(id)
	assert.Equal(t, ok, true)
	t.Log("Val: ", string(v))

	// 删除key
	err = idx.Delete([]byte("test1"))
	assert.NoError(t, err)

	// 查看删除后的值是否存在
	v, ok = idx.Val(id)
	assert.Equal(t, ok, false)
	t.Log("Val: ", string(v))
}

// Test_BigEndian 大端法小端法本质就是转成二进制
func Test_BigEndian(t *testing.T) {
	// int64 		8字节
	// int32        4字节
	// int16        2字节
	var val int32 = 256 // 2^8
	key := make([]byte, 4)
	binary.BigEndian.PutUint32(key[:], uint32(val))
	t.Log("Put: ", key)

	id := binary.BigEndian.Uint32(key)
	t.Log("U: ", id)

	// 测试任意长度
	key = make([]byte, 12)
	binary.BigEndian.PutUint32(key[:], uint32(val))
	t.Log("Put2: ", key)

	id = binary.BigEndian.Uint32(key)
	t.Log("U2: ", id)
}
