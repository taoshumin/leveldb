module gitlab.com/taoshumin/leveldb

go 1.18

require (
	github.com/greatroar/blobloom v0.7.2
	github.com/stretchr/testify v1.8.0
	github.com/syndtr/goleveldb v1.0.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/onsi/gomega v1.23.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
